package com.example.member_api;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Club {
    String id;
    String name;
    List<String> members;

    @JsonCreator
    public Club(@JsonProperty("id") String id,
                @JsonProperty("name") String name,
                @JsonProperty("members") List<String> members) {
        this.id = id;
        this.name = name;
        this.members = members;
    }
}
