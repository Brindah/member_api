package com.example.member_api;
import lombok.Value;
import java.util.List;

@Value
public class MemberDto {
        String id;
        String firstName;
        String lastName;
        String email;
        String userName;
        List<String> clubs;
}
