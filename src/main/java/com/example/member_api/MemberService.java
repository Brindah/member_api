package com.example.member_api;

import com.example.member_api.KeycloakCreateUser.KeycloakAdminClientService;
import com.example.member_api.jwt.KeycloakToken;
import lombok.AllArgsConstructor;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class MemberService {

    @Autowired
    MemberRepository memberRepository;
    @Autowired
    ClubRemoteApi clubRemoteApi;
    @Autowired
    KeycloakAdminClientService keycloakAdminClientService;

    public MemberEntity findMemberById(String memberId) throws MemberNotFoundException {
        try {
            return memberRepository.getById(memberId);
        } catch (EntityNotFoundException e) {
            throw new MemberNotFoundException(memberId);
        }
    }

    public List<MemberEntity> getAllMembers() {
        return memberRepository.findAll();
    }

    public boolean createMember(String firstName, String lastName, String email, String userName, String password) {

        UserRepresentation user = keycloakAdminClientService.createKeycloakUser(firstName,lastName,email,userName,password);
        if (user == null)
            return false;

        System.out.println(user.getId());
        MemberEntity memberEntity = new MemberEntity(
                user.getId(),
                firstName,
                lastName,
                email,
                userName,
                null
        );
        memberRepository.save(memberEntity);
        return true;
    }

    public void deleteMember(String memberId) throws MemberNotFoundException {
        MemberEntity member = findMemberById(memberId);
        member.removeAllClubs();
        clubRemoteApi.removeMemberFromClub(memberId);
        memberRepository.delete(member);
    }

    public List<MemberEntity> findMemberByName(String firstname, String lastName) {
        return memberRepository.findMemberEntitiesByFirstNameContainingOrLastNameContaining(firstname, lastName);
    }

    public List<Club> getAllClubs() throws Exception {
        return clubRemoteApi.getAllClubs();
    }

    public Club getClubById(String clubId) {
        return clubRemoteApi.getClubById(clubId);
    }

    public Club createClub(String clubName) {
        return clubRemoteApi.createAClub(clubName);
    }

    public MemberEntity joinAClub(String clubId, String memberId) throws Exception {
        return clubRemoteApi.joinAClub(memberId, clubId);
    }

    public Club updateClubName(String clubId, String newName) {
        return clubRemoteApi.updateClubName(clubId, newName);
    }

    public void deleteClub(String clubId) {
        clubRemoteApi.deleteClub(clubId);
    }

    public String getToken(String userName, String password) {
        KeycloakToken token = KeycloakToken
                .acquire("http://localhost:8180/", "realm1", "member_api", userName, password)
                .block();
        if (token == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        System.out.println("******************************************************************");
        System.out.println("YOU ARE LOGGED WITH TOKEN:  " + token);
        System.out.println("THIS IS YOUR REFRESHERTOKEN:  " + token.getRefreshToken());
        System.out.println("******************************************************************");
        return token.getAccessToken();
    }


}
