package com.example.member_api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "MEMBER_NOT_FOUND")
public class MemberNotFoundException extends Exception {
    public MemberNotFoundException(String s) {
        super(s);
    }
}
