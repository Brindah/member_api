package com.example.member_api;

import com.example.member_api.KeycloakCreateUser.KeycloakAdminClientService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("members")
@AllArgsConstructor
public class MemberController {

    @Autowired
    MemberRepository memberRepository;
    @Autowired
    MemberService memberService;
    @Autowired
    KeycloakAdminClientService keycloakAdminClientService;


    @GetMapping("/get_token")
    public String getToken(@RequestHeader(value = "userName") String userName,
                           @RequestHeader(value = "password") String password) {
        return memberService.getToken(userName, password);
    }

    @GetMapping("/get_string")
    public String getPrincipal(Principal principal) {
        System.out.println("****************PRINCIPAL*****************");
        System.out.println(principal);
        System.out.println(principal.getName());
        System.out.println("****************PRINCIPAL*****************");
        return principal.getName();
    }

   @GetMapping("/get_keycloak_user")
    public ResponseEntity getKeycloakUser(@RequestParam(value = "firstName")String firstName,
                                          @RequestParam(value = "lastName")String lastName,
                                          @RequestParam(value = "email")String email,
                                          @RequestParam(value = "userName")String userName,
                                          @RequestParam(value = "password")String password){
       boolean success = memberService.createMember(firstName, lastName, email, userName, password);
       return success ? ResponseEntity.ok("User created") : ResponseEntity.notFound().build();
    }


    @GetMapping("/get_all")
    public List<MemberDto> getAllMembers() {
        return memberService.getAllMembers()
                .stream()
                .map(MemberController::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{memberId}")
    public MemberDto getById(@PathVariable String memberId) throws MemberNotFoundException, IllegalAccessException {
        return toDTO(memberService.findMemberById(memberId));
    }

  /* @PostMapping("/create_member")
    public MemberDto createMember(@RequestParam String firstName,
                                  @RequestParam String lastName) {
        return toDTO(memberService.createMember(firstName, lastName));
    }*/

    @DeleteMapping("/delete_member/{memberId}")
    public void deleteMember(@PathVariable("memberId") String memberId) throws MemberNotFoundException {
        memberService.deleteMember(memberId);
    }

    @GetMapping("/get_all_clubs")
    public List<Club> getAllClubs() throws Exception {
        return memberService.getAllClubs();
    }

    @GetMapping("/get_club/{clubId}")
    public Club getClubById(@PathVariable("clubId") String clubId) {
        return memberService.getClubById(clubId);
    }

    @PostMapping("/create_club")
    public Club createClub(@RequestParam(value = "clubName") String clubName) {
        return memberService.createClub(clubName);
    }

    @GetMapping("/join_Club/{clubId}")
    public MemberDto joinAClub(@PathVariable("clubId") String clubId,
                               @RequestParam(value = "memberId") String memberId) throws Exception {
        return toDTO(memberService.joinAClub(clubId, memberId));
    }


    @PutMapping("/update_club_name/{clubId}")
    public Club updateClubName(@PathVariable("clubId") String clubId,
                               @RequestParam(value = "newName") String newName) {
        return memberService.updateClubName(clubId, newName);
    }

    @DeleteMapping("/delete_club/{clubId}")
    public void deleteClub(@PathVariable("clubId") String clubId) {
        memberService.deleteClub(clubId);
    }


    public static MemberDto toDTO(MemberEntity memberEntity) {
        return new MemberDto(
                memberEntity.getId(),
                memberEntity.getFirstName(),
                memberEntity.getLastName(),
                memberEntity.email,
                memberEntity.userName,
                memberEntity.getClubs());
    }

}
