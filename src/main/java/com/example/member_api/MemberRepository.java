package com.example.member_api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity,String> {
    List<MemberEntity> findMemberEntitiesByFirstNameContainingOrLastNameContaining(String firstName, String lastName);

}
