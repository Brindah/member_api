package com.example.member_api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "MEMBER_ALREADY_ADDED")
public class AlreadyAMemberException extends Exception {
    public AlreadyAMemberException(String s) {
        super(s);
    }
}
