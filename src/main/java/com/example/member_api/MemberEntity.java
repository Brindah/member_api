package com.example.member_api;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity(name = "member")
@Data
@NoArgsConstructor
public class MemberEntity {
    @Id
    String id;

    @Column
    String firstName;

    @Column
    String lastName;

    @Column
    String email;

    @Column
    String userName;

    @Column
    @ElementCollection
    List<String> clubs;

    public MemberEntity(String id, String firstName, String lastName, String email, String userName, List<String> clubs) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.userName = userName;
        this.clubs = clubs;
    }

    public void addClub(String clubId) throws AlreadyAMemberException {
        if (clubs.contains(clubId)) {
            throw new AlreadyAMemberException("THIS USER IS ALREADY A MEMBER OF THIS CLUB");
        }
        clubs.add(clubId);
    }

    public void removeAllClubs() {
        clubs.clear();
    }

}
