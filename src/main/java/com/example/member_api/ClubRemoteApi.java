package com.example.member_api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Component
public class ClubRemoteApi {

    //WebClient.Builder webclient;
    WebClient webClientCreate = WebClient.create("http://localhost:8084/clubs/");

    @Autowired
    MemberRepository memberRepository;

    public Club createAClub(String clubName) {
        return webClientCreate
                .post()
                .uri("create_club?clubName=" + clubName)
                .retrieve()
                .bodyToMono(Club.class)
                .block();
    }

    public MemberEntity joinAClub(String memberId, String clubId) throws Exception {
        MemberEntity member = memberRepository.getById(memberId);
        Club club = webClientCreate
                .get()
                .uri("add_member/" + clubId + "?memberId=" + memberId)
                .retrieve()
                .bodyToMono(Club.class)
                .block();

        if (club != null) {
            member.addClub(club.getId());
        } else throw new Exception("CLUB IS NULL");

        return memberRepository.save(member);
    }

    public List<Club> getAllClubs() throws Exception {
        List<Club> clubList = webClientCreate
                .get()
                .uri("all_clubs")
                .retrieve()
                .bodyToFlux(Club.class)
                .collectList()
                .block();

        assert clubList != null;
        if (clubList.isEmpty())
            throw new Exception("CLUBLIST IS EMPTY");
        return clubList;
    }

    public Club getClubById(String clubId) {
        return webClientCreate
                .get()
                .uri(clubId)
                .retrieve()
                .bodyToMono(Club.class)
                .block();
    }

    public Club updateClubName(String clubId, String newName) {
        return webClientCreate
                .put()
                .uri("update_club_name/" + clubId + "/?newName=" + newName)
                .retrieve()
                .bodyToMono(Club.class)
                .block();
    }

    public void removeMemberFromClub(String memberId) {
        webClientCreate
                .put()
                .uri("remove_member/?memberId" + memberId)
                .retrieve()
                .bodyToMono(void.class)
                .block();

    }

    public void deleteClub(String clubId) {
        webClientCreate
                .delete()
                .uri("delete_club/" + clubId)
                .retrieve()
                .bodyToMono(void.class)
                .block();
    }

    public static void main(String[] args) throws Exception {
/*
        Club club= WebClient.create("http://localhost:8084/clubs/create_club?clubName=dodo")
                .post()
                .retrieve()
                .bodyToMono(Club.class)
                .block();*/

     /*   List<Club> clubList = WebClient.create("http://localhost:8084/clubs/all_clubs")
                .get()
                .retrieve()
                .bodyToFlux(Club.class)
                .collectList()
                .block();*/

       /* Club club = WebClient.create("http://localhost:8084/clubs/update_club_name/5f0d402b-6a94-4938-b5f2-516c404685cf?newName=pppppp")
                .put()
                .retrieve()
                .bodyToMono(Club.class)
                .block();*/

       /* Club club = WebClient.create("http://localhost:8084/clubs/5f0d402b-6a94-4938-b5f2-516c404685cf")
                .get()
                .retrieve()
                .bodyToMono(Club.class)
                .block();*/

/*
        Club club = WebClient.create("http://localhost:8084/clubs/add_member/5f0d402b-6a94-4938-b5f2-516c404685cf?memberId=26dea71e-a07c-490f-b44f-bb969b00ca43")
                .get()
                .retrieve()
                .bodyToMono(Club.class)
                .block();


        System.out.println("************************CLUB: " + club);*/
    }


}
